package ru.god.statistics;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, предназначенный для анализа и сбора статистики по заданному тексту, а именно:
 * подсчет общего количества символов, количества символов без учета пробелов и подсчет количества слов
 *
 * @author Горбачева, 16ИТ18к
 */
public class Statistics {
    public static void main(String[] args) {
        System.out.println("Введите, пожалуйста, полное имя файла, анализ которого необходимо" +
                " произвести, например, следующим образом: D:\\work\\StatisticsOfText\\text.txt");
        String[] strings = readStrings(pathToFile());
        if (strings != null) {
            int allSymbols = allSymbolsCounter(strings);
            int symbolsWithoutSpaces = allSymbols - spacesCounter(strings);
            int words = wordsCounter(strings);
            System.out.printf("Количество символов в полученном тексте равно %d" +
                            "%nТабуляция и переходы на новую строку не учитываются" +
                            "%nКоличетво символов без учета пробелов равно %d" +
                            "%nКоличество слов на русском языке в полученном текте равно %d", allSymbols,
                    symbolsWithoutSpaces, words);
        }
    }

    /**
     * Возвращает полное имя файла, указанное пользователем
     *
     * @return строка с полным именем файла
     */
    private static String pathToFile() {
        return new Scanner(System.in).nextLine();
    }

    /**
     * Возвращает прочитанный из файла текст в виде массива строк
     *
     * @return массив String[]  исходными данными
     */
    private static String[] readStrings(String pathToFile) {
        List<String> arrayList;
        try {
            arrayList = Files.readAllLines(Paths.get(pathToFile));
        } catch (IOException e) {
            System.out.println("Произошла ошибка, проверьте правильность вводимых данных и попробуйте еще раз");
            return null;
        }
        return arrayList.toArray(new String[0]);
    }

    /**
     * Возвращает число, укаывающее, сколько символов содержится в полученном тексте
     *
     * @param strings - массив с исходными данными
     * @return общее количество символов
     */
    private static int allSymbolsCounter(String[] strings) {
        int result = 0;
        for (String string : strings) {
            result += string.length();
        }
        return result;
    }

    /**
     * Возвращает число, укаывающее, сколько пробелов содержится в полученном тексте
     *
     * @param strings - массив с исходными данными
     * @return количество пробелов
     */
    private static int spacesCounter(String[] strings) {
        int result = 0;
        Pattern pattern = Pattern.compile(" ");
        Matcher matcher;
        for (String string : strings) {
            matcher = pattern.matcher(string);
            while (matcher.find()) {
                result++;
            }
        }
        return result;
    }

    /**
     * Возвращает число, указывающее, сколько слов содержится в полученном тексте
     *
     * @param strings - массив с исходными данными
     * @return количество слов
     */
    private static int wordsCounter(String[] strings) {
        int result = 0;
        Pattern pattern = Pattern.compile("([А-Яа-я0-9]+(([\\-]|[.])[А-Яа-я0-9]+)*)|\\d+");
        Matcher matcher;
        for (String string : strings) {
            matcher = pattern.matcher(string);
            while (matcher.find()) {
                result++;
            }
        }
        return result;
    }
}
